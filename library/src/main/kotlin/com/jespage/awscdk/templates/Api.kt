package com.jespage.awscdk.templates

import com.jespage.awscdk.build
import software.amazon.awscdk.CfnOutput
import software.amazon.awscdk.Stack
import software.amazon.awscdk.services.apigatewayv2.CfnApi
import software.amazon.awscdk.services.apigatewayv2.CfnApiMapping
import software.amazon.awscdk.services.apigatewayv2.CfnDomainName
import software.amazon.awscdk.services.cloudfront.*
import software.amazon.awscdk.services.cloudfront.origins.HttpOrigin
import software.amazon.awscdk.services.lambda.CfnPermission

private const val STAGE_NAME = "\$default"

data class ApiCustomDomainName(
    val certificateArn: String,
    val domainName: String
)

@Suppress("unused")
fun Stack.api(
    lambdaFunctionArn: String,
    resPrefix: String = "",
    customDomainName: ApiCustomDomainName? = null,
): CfnApi {
    val api = CfnApi.Builder.create(this, "${resPrefix}Api").build {
        name("api")
        description("Api")
        protocolType("HTTP")
        target(lambdaFunctionArn)
    }
    CfnPermission.Builder.create(this, "${resPrefix}LambdaApiPermission").build {
        action("lambda:invokeFunction")
        functionName(lambdaFunctionArn)
        principal("apigateway.amazonaws.com")
        sourceArn("arn:aws:execute-api:${region}:${account}:${api.ref}/*/$STAGE_NAME")
    }
    if (customDomainName != null) {
        val domainName = CfnDomainName.Builder.create(this, "${resPrefix}ApiDomainName").build {
            domainName(customDomainName.domainName)
            domainNameConfigurations(
                listOf(
                    CfnDomainName.DomainNameConfigurationProperty.builder().build {
                        it.certificateArn(customDomainName.certificateArn)
                        it.endpointType("REGIONAL")
                        it.securityPolicy("TLS_1_2")
                    }
                )
            )
        }
        CfnApiMapping.Builder.create(this, "${resPrefix}ApiDomainNameMapping").build {
            domainName(domainName.ref)
            apiId(api.ref)
            stage(STAGE_NAME)
        }
    }
    CfnOutput.Builder.create(this, "${resPrefix}ApiEndpoint").value(api.attrApiEndpoint).build()
    return api
}

@Suppress("unused")
fun Stack.apiDistributionBehaviour(
    api: CfnApi,
    path: String = "/api/*",
    requestPolicy: OriginRequestPolicy? = null,
    behaviourBuilder: BehaviorOptions.Builder.() -> Unit = {}
): Pair<String, BehaviorOptions> {
    return path to BehaviorOptions.builder().build {
        it.origin(HttpOrigin.Builder.create("${api.ref}.execute-api.${region}.amazonaws.com").build {
            it.protocolPolicy(OriginProtocolPolicy.HTTPS_ONLY)
        })
        it.allowedMethods(AllowedMethods.ALLOW_ALL)
        it.viewerProtocolPolicy(ViewerProtocolPolicy.HTTPS_ONLY)
        it.cachePolicy(CachePolicy.CACHING_DISABLED)
        it.originRequestPolicy(requestPolicy ?: OriginRequestPolicy.ALL_VIEWER_EXCEPT_HOST_HEADER)
        it.behaviourBuilder()
    }
}