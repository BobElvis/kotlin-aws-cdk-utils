@file:Suppress("LeakingThis")

package com.jespage

import org.gradle.api.DefaultTask
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction
import java.io.File

abstract class AwsCdkDefaultTask : DefaultTask() {
    @get:Input
    @get:Optional
    abstract val awsProfile: Property<String>

    @get:Input
    abstract val outDir: Property<String>

    init {
        group = "aws"
    }
}

abstract class AwsCdkSynth : AwsCdkDefaultTask() {
    @TaskAction
    fun synth() {
        val gradle = project.gradle.gradleHomeDir?.let { File(it, "bin/gradle") }
            ?: throw Exception("Missing gradle home information.")
        println("== START: Synth ==")
        awsCdkCmd(
            "${gradle.path} run",
            awsProfile.orNull,
            "synth",
            "--output", outDir.get()
        )
        println("== END: Synth ==")
    }
}

abstract class AwsCdkBootstrap : AwsCdkDefaultTask() {
    @get:Input
    @get:Optional
    abstract val account: Property<String>

    @get:Input
    @get:Optional
    abstract val region: Property<String>

    @TaskAction
    fun bootstrap() = awsCdkCmd(outDir, awsProfile, "bootstrap") {
        if (account.isPresent || region.isPresent) {
            if (account.isPresent != region.isPresent) throw Exception("Must specify account and region, or neither.")
            add("${account.get()}/${region.get()}")
        }
    }
}

abstract class AwsCdkDiff : AwsCdkDefaultTask() {
    @TaskAction
    fun diff() = awsCdkCmd(outDir, awsProfile, "diff")
}

abstract class AwsCdkDeploy : AwsCdkDefaultTask() {
    @get:Input
    abstract val stackNames: ListProperty<String>

    @get:Input
    abstract val parameters: MapProperty<String, String>

    init {
        parameters.convention(emptyMap())
    }

    @TaskAction
    fun deployStack() = awsCdkCmd(
        outDir, awsProfile, "deploy",
        *stackNames.get().toTypedArray(),
        *parameters.get().flatMap { (key, value) ->
            listOf("--parameters", "$key=$value")
        }.toTypedArray(),
        "--require-approval", "never"
    )
}
