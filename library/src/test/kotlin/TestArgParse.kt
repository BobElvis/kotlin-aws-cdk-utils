import com.jespage.awscdk.getArgValues
import com.jespage.awscdk.requireArg
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class TestArgParse {
    @Test
    fun testArgParse() {
        val args = arrayOf(
            "a", "b",
            "--c", "yo",
            "--d",
            "--e", "hello", "world"
        )

        // Positional:
        assertEquals("a", args.requireArg(0))
        assertEquals("b", args.requireArg(1))
        assertFailsWith<NoSuchElementException> { args.requireArg(2) }
        assertFailsWith<NoSuchElementException> { args.requireArg(3) }

        // Arg values:
        assertEquals("yo", args.requireArg("c"))
        val c by args.requireArg()
        assertEquals("yo", c)
        assertEquals(emptyList(), args.getArgValues("d"))
        assertEquals(listOf("hello", "world"), args.getArgValues("e"))
        assertEquals(emptyList(), args.getArgValues("f"))
        assertFailsWith<NoSuchElementException> { args.requireArg("notThere") }
        assertFailsWith<NoSuchElementException> { args.requireArg("e") }
    }
}