package com.jespage.awscdk.templates

import com.jespage.awscdk.build
import policyStatement
import software.amazon.awscdk.CfnOutput
import software.amazon.awscdk.RemovalPolicy
import software.amazon.awscdk.Stack
import software.amazon.awscdk.services.certificatemanager.Certificate
import software.amazon.awscdk.services.cloudfront.*
import software.amazon.awscdk.services.cloudfront.origins.S3Origin
import software.amazon.awscdk.services.iam.AnyPrincipal
import software.amazon.awscdk.services.route53.CfnRecordSet
import software.amazon.awscdk.services.s3.BlockPublicAccess
import software.amazon.awscdk.services.s3.Bucket
import software.amazon.awscdk.services.s3.deployment.BucketDeployment
import software.amazon.awscdk.services.s3.deployment.CacheControl
import software.amazon.awscdk.services.s3.deployment.Source

private const val CLOUD_FRONT_HOSTED_ZONE_ID = "Z2FDTNDATAQYW2"

enum class SpaStrategy { S3, CLOUD_FRONT }
data class CacheOptions(
    val serverMaxAgeSeconds: Int = 86400,
    val localMaxAgeSeconds: Int = 15
) {
    override fun toString(): String = "s-maxage=$serverMaxAgeSeconds, max-age=$localMaxAgeSeconds"
}

/**
 * Parameters for a custom domain name.
 * @param domainName The alternate domain name of the distribution.
 * @param certificateArn The certificate arn for the domain name.
 * @param domainHostedZoneId The hosted zone of the domain name (if the domain name is hosted in AWS). This will
 * provision a redirect to the Cloud Front distribution.
 */
data class WebsiteDomainArgs(
    val domainName: String,
    val certificateArn: String,
    val domainHostedZoneId: String?,
)

fun Stack.publicBucket(
    resourceName: String,
    websiteRedirect: Boolean = true,
    requireReferer: String? = null,
    builder: Bucket.Builder.() -> Unit = {}
): Bucket = Bucket.Builder.create(this, resourceName).build {
    publicReadAccess(true)
    blockPublicAccess(BlockPublicAccess.BLOCK_ACLS)
    versioned(false)
    removalPolicy(RemovalPolicy.DESTROY)
    autoDeleteObjects(true)
    if (websiteRedirect) {
        websiteIndexDocument("index.html")
        websiteErrorDocument("index.html")
    }
    builder()
}.also {
    if (requireReferer != null) it.addToResourcePolicy(policyStatement {
        principals(AnyPrincipal())
        actions("s3:getObject")
        resources(it.arnForObjects("*"))
        condition("StringEquals", "aws:Referer", requireReferer)
    })
}

fun Stack.websiteBucketDeployment(
    resourceName: String,
    bucket: Bucket,
    distribution: Distribution,
    staticResourcesFolder: String,
    cacheOptions: CacheOptions = CacheOptions()
): BucketDeployment = BucketDeployment.Builder.create(this, resourceName).build {
    it.destinationBucket(bucket)
    it.cacheControl(listOf(CacheControl.fromString(cacheOptions.toString())))
    it.distribution(distribution)
    it.sources(listOf(Source.asset(staticResourcesFolder)))
}

fun Stack.websiteDistribution(
    resourceName: String,
    staticBucket: Bucket,
    staticBucketReferer: String,
    domainName: String?,
    certificateArn: String?,
    spaRedirect: Boolean = false,
    description: String? = null,
    builder: Distribution.Builder.() -> Unit = {}
): Distribution {
    val certRes = certificateArn?.let {
        Certificate.fromCertificateArn(this, "${resourceName}Cert", certificateArn)
    }
    return Distribution.Builder.create(this, resourceName).build {
        description?.let { comment(it) }
        defaultRootObject("index.html")
        defaultBehavior(BehaviorOptions.builder().build {
            it.origin(S3Origin.Builder.create(staticBucket).build {
                it.customHeaders(mapOf("Referer" to staticBucketReferer))
            })
            it.cachePolicy(CachePolicy.CACHING_OPTIMIZED)
            it.viewerProtocolPolicy(ViewerProtocolPolicy.REDIRECT_TO_HTTPS)
        })
        if (spaRedirect) {
            errorResponses(listOf(
                ErrorResponse.builder().build {
                    it.httpStatus(404)
                    it.responseHttpStatus(200)
                    it.responsePagePath("/index.html")
                }
            ))
        }
        domainName?.let { domainNames(listOf(it)) }
        certRes?.let { certificate(it) }
        priceClass(PriceClass.PRICE_CLASS_100)
        sslSupportMethod(SSLMethod.SNI)
        httpVersion(HttpVersion.HTTP2_AND_3)
        minimumProtocolVersion(SecurityPolicyProtocol.TLS_V1_2_2021)
        builder()
    }
}

fun Stack.websiteDomainRedirect(
    resourceName: String,
    domainName: String,
    domainHostedZoneId: String,
    distribution: Distribution
): CfnRecordSet = CfnRecordSet.Builder.create(this, resourceName).build {
    name(domainName)
    type("A")
    hostedZoneId(domainHostedZoneId)
    aliasTarget(CfnRecordSet.AliasTargetProperty.builder().build {
        it.hostedZoneId(CLOUD_FRONT_HOSTED_ZONE_ID)
        it.dnsName(distribution.distributionDomainName)
    })
}


/**
 * Typical resource path: "../software/build/distributions/js"
 */
@Suppress("unused")
fun Stack.website(
    distributionDescription: String?,
    domain: WebsiteDomainArgs?,
    staticResourcesFolder: String,
    bucketRefererSecret: String,
    resourcePrefix: String = "",
    spaStrategy: SpaStrategy = SpaStrategy.S3,
    staticCacheOptions: CacheOptions = CacheOptions(),
    bucketBuilder: Bucket.Builder.() -> Unit = {},
    distributionBuilder: Distribution.Builder.() -> Unit = {}
): Distribution {
    val staticBucket = publicBucket(
        resourceName = "${resourcePrefix}StaticBucket",
        websiteRedirect = spaStrategy == SpaStrategy.S3,
        requireReferer = bucketRefererSecret,
        builder = bucketBuilder
    )
    val distribution = websiteDistribution(
        description = distributionDescription,
        resourceName = "${resourcePrefix}Distribution",
        staticBucket = staticBucket,
        staticBucketReferer = bucketRefererSecret,
        domainName = domain?.domainName,
        certificateArn = domain?.certificateArn,
        spaRedirect = spaStrategy == SpaStrategy.CLOUD_FRONT,
        builder = distributionBuilder
    )
    if (domain?.domainHostedZoneId != null) {
        websiteDomainRedirect(
            resourceName = "${resourcePrefix}DomainRecord",
            domainName = domain.domainName,
            domainHostedZoneId = domain.domainHostedZoneId,
            distribution = distribution
        )
    }
    websiteBucketDeployment(
        resourceName = "${resourcePrefix}StaticBucketDeployment",
        bucket = staticBucket,
        distribution = distribution,
        staticResourcesFolder = staticResourcesFolder,
        cacheOptions = staticCacheOptions
    )
    CfnOutput.Builder.create(this, "${resourcePrefix}CloudfrontEndpoint").value(distribution.domainName).build()
    return distribution
}