# kotlin-aws-cdk-utils

Consists of three AWS projects:

* Gradle plugin for building aws lambda apps:
    * Configuring a single AWS http client.
    * Configuring a lambda deployment zip.
* Gradle plugin for building cdk apps.
* Utilities for building of a cdk app with kotlin.

Check the package registry for latest versions.

### Plugins

Plugins need to define the repository in settings.

`settings.gradle.kts`

```
pluginManagement {
    repositories {
        gradlePluginPortal()
        maven("https://gitlab.com/api/v4/projects/41735566/packages/maven")
    }
}
```

### AWS SDK Plugin

```
plugins {
    id("com.jespage.awssdk") version "<pluginVersion>"
}
```

### AWS CDK Plugin

The plugin needs the _application_ plugin and applies it by default.

`build.gradle.kts`

```
plugins {
    id("com.jespage.awscdk") version "<pluginVersion>"
}
```

### Library

If using the plugin, one can omit the repository setup.

`build.gradle.kts:`

```
repositories {
    maven("https://gitlab.com/api/v4/projects/41735566/packages/maven")
}

dependencies {
    implementation("com.jespage:kotlin-aws-cdk-utils:<utilsVersion>")
    implementation("software.amazon.awscdk:aws-cdk-lib:<awsCdkVersion>")
}
```