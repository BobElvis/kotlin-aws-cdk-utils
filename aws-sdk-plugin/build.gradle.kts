plugins {
    `java-gradle-plugin`
    `kotlin-dsl`  // needed for org.gradle.kotlin.dsl
}

version = "1.4.4"

gradlePlugin {
    plugins {
        create("plugin-sdk") {
            id = "$group.awssdk"
            implementationClass = "$group.AwsSdkPlugin"
        }
    }
}