package com.jespage.awscdk

import kotlin.properties.ReadOnlyProperty

fun Array<String>.requireArg() =
    ReadOnlyProperty<Nothing?, String> { _, property -> requireArg(property.name) }

fun Array<String>.requireArg(pos: Int): String {
    forEachIndexed { index, s ->
        if (s.startsWith("--")) throw NoSuchElementException("No positional argument found at index $pos.")
        if (index == pos) return s
    }
    throw NoSuchElementException("No positional argument found at index $pos.")
}

fun Array<String>.requireArg(name: String) =
    getArgValues(name).takeIf { it.size == 1 }?.get(0).takeUnless { it.isNullOrBlank() }
        ?: throw NoSuchElementException("No values given for --$name.")

fun Array<String>.getArgValues(name: String) = buildList {
    var found = false
    this@getArgValues.forEach {
        when {
            found && it.startsWith("--") -> return@buildList
            found -> add(it)
            it == "--$name" -> found = true
        }
    }
}
