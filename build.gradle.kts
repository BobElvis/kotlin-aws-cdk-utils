subprojects {
    apply(plugin = "org.gradle.maven-publish")
    repositories { mavenCentral() }
    group = "com.jespage"
    extensions.configure<PublishingExtension>("publishing") {
        repositories {
            maven {
                val projectId = System.getenv("CI_PROJECT_ID")
                url = uri("https://gitlab.com/api/v4/projects/${projectId}/packages/maven")
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class)
                }
            }
        }
    }
}