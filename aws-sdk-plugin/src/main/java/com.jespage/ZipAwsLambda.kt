package com.jespage

import org.gradle.api.tasks.bundling.Zip

@Suppress("unused", "LeakingThis")
abstract class ZipAwsLambdaBase(taskName: String) : Zip() {
    init {
        group = "distribution"
        archiveFileName.convention("lambda.zip")
        from(project.tasks.named(taskName))
        from(project.tasks.named("processResources"))
        exclude("caches-jvm", "last-build.bin")
        into("lib") {
            from(project.configurations.named("runtimeClasspath"))
        }
    }
}

@Suppress("unused")
abstract class ZipAwsLambda : ZipAwsLambdaBase("compileKotlin")

@Suppress("unused")
abstract class ZipAwsLambdaMultiplatform : ZipAwsLambdaBase("compileKotlinJvm")