import com.jespage.awscdk.*
import software.amazon.awscdk.services.secretsmanager.CfnSecret
import kotlin.test.Test

class TestBuilderApp {
    @Test
    fun testBuilderApp() {
        app {
            stack("hello-world") {
                CfnSecret.Builder.create(this, "TestSecret").build {
                    description("Hello world.")
                    tags(tagsOf("hello" to "world"))
                    tags(mapOf("hello" to "world").toTags())
                }
            }
        }
    }
}
