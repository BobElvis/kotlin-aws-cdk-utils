package com.jespage

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaApplication
import org.gradle.api.provider.Property
import org.gradle.api.tasks.JavaExec
import org.gradle.kotlin.dsl.*

interface AwsCdkPluginExtension {
    val mainClass: Property<String>
    val outDir: Property<String>
    val defaultAwsProfile: Property<String>

    @Suppress("unused")
    fun Project.synth(configuration: JavaExec.() -> Unit) =
        tasks.named<JavaExec>("run").configure(configuration)

    @Suppress("unused")
    fun Project.deploy(configuration: AwsCdkDeploy.() -> Unit) =
        tasks.named<AwsCdkDeploy>("deploy").configure(configuration)
}

@Suppress("unused")
class AwsCdkPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        // Configure extension:
        project.plugins.apply("org.gradle.application")

        val extension = project.extensions.create<AwsCdkPluginExtension>("aws-cdk").apply {
            mainClass.convention("CdkAppKt")
            outDir.convention("${project.buildDir}/aws-cdk")
        }

        // Configure the repo for the utils library.
        project.repositories {
            maven("https://gitlab.com/api/v4/projects/41735566/packages/maven")
        }

        // Configure synth:
        project.extensions.configure<JavaApplication>("application") {
            mainClass.set(extension.mainClass)
        }
        val synth = project.tasks.register<AwsCdkSynth>("synth")

        // Bootstrap
        val bootstrap = project.tasks.register<AwsCdkBootstrap>("bootstrap") {
            dependsOn(synth)
        }

        // List:
        project.tasks.register("list") {
            group = "aws"
            dependsOn(synth)
            doLast { awsCdkCmd(extension.outDir, null, "ls") }
        }

        // Configure deploy
        project.tasks.register<AwsCdkDeploy>("deploy") {
            description = "Deploys all stacks."
            stackNames.set(listOf("\"*\""))
        }
        project.tasks.withType<AwsCdkDeploy> { dependsOn(bootstrap) }

        // Configure diff
        project.tasks.register<AwsCdkDiff>("diff")
        project.tasks.withType<AwsCdkDiff> { dependsOn(bootstrap) }

        // Configure conventions from extension
        project.tasks.withType<AwsCdkDefaultTask> {
            outDir.convention(extension.outDir)
            awsProfile.convention(extension.defaultAwsProfile)
        }
    }
}
