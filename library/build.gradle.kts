plugins {
    kotlin("jvm") version "2.0.0"
    `java-library`
}

val id = "kotlin-aws-cdk-utils"
version = "1.3.6"

java {
    withJavadocJar()
    withSourcesJar()
}

kotlin {
    jvmToolchain(11)
}

dependencies {
    api("software.amazon.awscdk:aws-cdk-lib:2.144.0")
    testImplementation(kotlin("test"))
}

tasks.jar {
    manifest {
        attributes(
            mapOf(
                "Implementation-Title" to id,
                "Implementation-Version" to project.version
            )
        )
    }
}

publishing {
    publications {
        create<MavenPublication>("library") {
            artifactId = id
            from(components["java"])
        }
    }
}
