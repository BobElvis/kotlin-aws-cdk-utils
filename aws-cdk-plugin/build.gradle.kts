plugins {
    `java-gradle-plugin`
    `kotlin-dsl`  // needed for org.gradle.kotlin.dsl
}

version = "1.0.7"

gradlePlugin {
    plugins {
        create("plugin-cdk") {
            id = "$group.awscdk"
            implementationClass = "$group.AwsCdkPlugin"
        }
    }
}
