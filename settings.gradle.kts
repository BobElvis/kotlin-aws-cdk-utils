plugins {
    id("org.gradle.toolchains.foojay-resolver-convention") version "0.5.0"
}

rootProject.name = "kotlin-aws-cdk-utils"
include("library", "aws-cdk-plugin", "aws-sdk-plugin")
