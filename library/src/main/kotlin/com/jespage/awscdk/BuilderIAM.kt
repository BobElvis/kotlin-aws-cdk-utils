@file:Suppress("unused", "CanBeParameter")

import com.jespage.awscdk.build
import software.amazon.awscdk.Stack
import software.amazon.awscdk.services.iam.*
import software.amazon.awscdk.services.iam.Role as AwsRole
import software.amazon.awscdk.services.iam.ServicePrincipal as AwsServicePrincipal

@DslMarker
annotation class BuilderMarker

@BuilderMarker
class PolicyStatementBuilder {
    var sid: String? = null
    var effect: Effect = Effect.ALLOW
    val actions = mutableListOf<String>()
    val resources = mutableListOf<String>()
    val principals = mutableListOf<IPrincipal>()
    val conditions = mutableMapOf<String, MutableMap<String, Any>>()
    fun actions(vararg actions: String) = this.actions.addAll(actions)
    fun resources(vararg resources: String) = this.resources.addAll(resources)
    fun principals(vararg principals: IPrincipal) = this.principals.addAll(principals)

    fun condition(operator: String, attribute: String, value: String, vararg values: String) {
        conditions.getOrPut(operator) { mutableMapOf() }[attribute] =
            if (values.isEmpty()) value else (listOf(value) + values)
    }
}

@BuilderMarker
open class PolicyBuilder {
    val statements = mutableListOf<PolicyStatementBuilder>()
    fun statement(builder: PolicyStatementBuilder.() -> Unit) {
        statements.add(PolicyStatementBuilder().apply(builder))
    }
}

@BuilderMarker
class PrincipalIamBuilder : PolicyBuilder() {
    val managedPolicyArns = mutableListOf<String>()
    fun managedPolicyArn(arn: String) = run { managedPolicyArns.add(arn) }
}

sealed interface ServicePrincipal {
    val services: List<String>

    open class Single(val service: String) : ServicePrincipal {
        override val services = listOf(service)
    }

    class Multi(vararg services: String) : ServicePrincipal {
        constructor(vararg services: Single) : this(*services.map { it.service }.toTypedArray())

        override val services = services.toList()
    }

    object Lambda : Single("lambda.amazonaws.com")
    object EC2 : Single("ec2.amazonaws.com")
}

fun Stack.roleCfn(
    roleName: String,
    assumeServicePrincipal: ServicePrincipal,
    resourceName: String? = null,
    builder: PrincipalIamBuilder.() -> Unit
): CfnRole {
    val assumeRolePolicyDocument = PolicyDocument.Builder.create().build { doc ->
        doc.statements(listOf(PolicyStatement.Builder.create().build {
            it.effect(Effect.ALLOW)
            it.actions(listOf("sts:AssumeRole"))
            it.principals(assumeServicePrincipal.services.map { AwsServicePrincipal.Builder.create(it).build() })
        }))
    }
    val role = PrincipalIamBuilder().apply(builder)
    return CfnRole.Builder.create(this, resourceName ?: roleName).build {
        roleName(roleName)
        assumeRolePolicyDocument(assumeRolePolicyDocument)
        path("/")
        role.toCfnPolicyDocument()?.let { doc ->
            policies(listOf(CfnRole.PolicyProperty.builder().build {
                it.policyName("policy")
                it.policyDocument(doc)
            }))
        }
        managedPolicyArns(role.managedPolicyArns)
    }
}

fun Stack.role(
    roleName: String,
    assumeServicePrincipal: ServicePrincipal,
    resourceName: String? = null,
    builder: PrincipalIamBuilder.() -> Unit
): AwsRole {
    val role = PrincipalIamBuilder().apply(builder)
    val resName = resourceName ?: roleName
    return AwsRole.Builder.create(this, resName).build {
        roleName(roleName)
        path("/")
        assumedBy(
            when (assumeServicePrincipal) {
                is ServicePrincipal.Multi -> {
                    CompositePrincipal(*assumeServicePrincipal.services.map {
                        AwsServicePrincipal.Builder.create(it).build()
                    }.toTypedArray())
                }
                is ServicePrincipal.Single -> {
                    AwsServicePrincipal.Builder.create(assumeServicePrincipal.services[0]).build()
                }
                else -> throw Exception()
            }
        )
        managedPolicies(role.managedPolicyArns.mapIndexed { index, s ->
            ManagedPolicy.fromManagedPolicyArn(this@role, "${resName}Managed$index", s)
        })
        role.toCfnPolicyDocument()?.let { inlinePolicies(mapOf("policy" to it)) }
    }
}

fun Stack.userCfn(
    username: String,
    resourceName: String? = null,
    builder: PrincipalIamBuilder.() -> Unit
): CfnUser {
    val user = PrincipalIamBuilder().apply(builder)
    return CfnUser.Builder.create(this, resourceName ?: username).build {
        userName(username)
        path("/")
        user.toCfnPolicyDocument()?.let { doc ->
            policies(listOf(CfnUser.PolicyProperty.builder().build {
                it.policyName("policy")
                it.policyDocument(doc)
            }))
        }
        managedPolicyArns(user.managedPolicyArns)
    }
}

fun Stack.managedPolicy(
    policyName: String,
    resourceName: String?,
    description: String,
    builder: PolicyBuilder.() -> Unit
): CfnManagedPolicy =
    CfnManagedPolicy.Builder.create(this, resourceName ?: policyName)
        .managedPolicyName(policyName)
        .path("/")
        .description(description)
        .policyDocument(policyDocument(builder))
        .build()

fun policyDocument(builder: PolicyBuilder.() -> Unit) =
    PolicyBuilder().apply(builder).toCfnPolicyDocument() ?: error("No statements specified.")

fun policyStatement(builder: PolicyStatementBuilder.() -> Unit) =
    PolicyStatementBuilder().apply(builder).toCfn()

private fun PolicyBuilder.toCfnPolicyDocument(): PolicyDocument? {
    val awsStatements = statements.map { it.toCfn() }
    return if (awsStatements.isEmpty()) null
    else PolicyDocument.Builder.create().statements(awsStatements).build()
}

private fun PolicyStatementBuilder.toCfn(): PolicyStatement =
    PolicyStatement.Builder.create().apply {
        sid?.let { sid(it) }
        effect(effect)
        actions(actions)
        resources(resources)
        principals(principals)
        conditions(conditions)
    }.build()
