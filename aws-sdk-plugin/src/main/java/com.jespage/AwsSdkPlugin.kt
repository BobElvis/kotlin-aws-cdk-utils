package com.jespage

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies
import org.gradle.kotlin.dsl.exclude
import org.gradle.kotlin.dsl.register

@Suppress("unused")
class AwsSdkPlugin : Plugin<Project> {
    override fun apply(target: Project) = Unit
}

enum class AwsHttpClient(val module: String) {
    CRT("aws-crt-client"),
    NETTY("netty-nio-client"),
    APACHE("apache-client")
}

@Suppress("unused")
fun Project.awsSdk(
    version: String,
    clientType: AwsHttpClient = AwsHttpClient.CRT,
    awsLambdaZip: Boolean = false
) {
    /** Remove unselected clients and add the selected **/
    val removeClients = AwsHttpClient.values().filter { it != clientType }
    fun configure(configName: String) = configurations.all {
        removeClients.forEach { exclude("software.amazon.awssdk", it.module) }
        if (name != configName) return@all

        /** Set BOM and selected client type **/
        dependencies {
            add(name, project.dependencies.platform("software.amazon.awssdk:bom:$version"))
            add(name, "software.amazon.awssdk:${clientType.module}")
        }
    }

    plugins.withId("org.jetbrains.kotlin.jvm") {
        println("[AwsSdkPlugin] Project is Kotlin JVM.")
        configure("implementation")
        if (awsLambdaZip) registerAwsLambdaZipTask<ZipAwsLambda>()
    }
    plugins.withId("org.jetbrains.kotlin.multiplatform") {
        println("[AwsSdkPlugin] Project is Kotlin Multiplatform.")
        configure("jvmMainImplementation")
        if (awsLambdaZip) registerAwsLambdaZipTask<ZipAwsLambdaMultiplatform>()
    }

    /** Add linux classifier for CRT so we kan keep that in Zip **/
    if (awsLambdaZip && clientType == AwsHttpClient.CRT) {
        val classifiers = listOf(null, "linux-x86_64")
        configurations.all {
            resolutionStrategy.eachDependency {
                if (requested.group != "software.amazon.awssdk.crt") return@eachDependency
                if (requested.name != "aws-crt") return@eachDependency
                artifactSelection {
                    classifiers.forEach {
                        selectArtifact("jar", null, it)
                    }
                }
            }
        }
    }
}

private inline fun <reified T : ZipAwsLambdaBase> Project.registerAwsLambdaZipTask() =
    tasks.register<T>("lambdaZip") {
        // Excludes aws-crt without classifier.
        exclude { Regex("aws-crt-[0-9.]*.jar").matches(it.name) }
    }