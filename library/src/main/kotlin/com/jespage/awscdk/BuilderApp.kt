package com.jespage.awscdk

import software.amazon.awscdk.*
import software.amazon.jsii.Builder

inline fun <S : CfnResource, T : Builder<S>> T.build(builder: T.() -> Unit): S = apply(builder).build()
inline fun <S : Resource, T : Builder<S>> T.build(builder: T.() -> Unit): S = apply(builder).build()
inline fun <S : Any, T : Builder<S>> T.build(builder: (T) -> Unit): S = also(builder).build()

inline fun app(outDir: String = "build/aws-cdk", block: App.() -> Unit) {
    App.Builder.create().build { it.outdir(outDir) }.apply(block).synth()
}

inline fun App.stack(
    name: String,
    account: String? = null,
    region: String? = null,
    description: String? = null,
    tags: Map<String, String> = emptyMap(),
    builder: Stack.() -> Unit
) {
    val stackProps = StackProps.builder().build {
        description?.run { it.description(this) }
        it.tags(tags)
        it.env(Environment.builder().build {
            account?.run { it.account(this) }
            region?.run { it.region(this) }
        })
    }
    Stack(this, name, stackProps).apply(builder)
}

fun Map<String, String>.toTags() = map { (key, value) ->
    CfnTag.builder().build { it.key(key); it.value(value) }
}

fun tagsOf(vararg tags: Pair<String, String>) = tags.map { (key, value) ->
    CfnTag.builder().build { it.key(key); it.value(value) }
}
