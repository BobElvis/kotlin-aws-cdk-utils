package com.jespage

import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.JavaExec
import org.gradle.api.tasks.TaskProvider
import org.gradle.api.tasks.bundling.Zip
import org.gradle.kotlin.dsl.named
import org.gradle.process.ExecResult
import java.lang.IllegalStateException

@Suppress("unused")
fun Project.awsCdk(configuration: AwsCdkPluginExtension.() -> Unit): Unit =
    extensions.configure<AwsCdkPluginExtension>("aws-cdk", configuration)

@Suppress("unused")
fun Project.zipTask(name: String) = tasks.named<Zip>(name)

@Suppress("unused")
fun TaskProvider<Zip>.outputPath(relativeTo: Project): String =
    get().archiveFile.get().asFile.relativeTo(relativeTo.projectDir).path

@Suppress("unused")
var JavaExec.namedArgs: Map<String, String>
    get() = throw IllegalStateException("Not supported.")
    set(value) = run { args = value.flatMap { listOf("--${it.key}", it.value) } }

fun Task.cmd(
    vararg args: String,
    environment: Map<String, String>,
    builder: MutableList<String>.() -> Unit = {}
) = project.exec {
    environment(environment)
    commandLine(args.toMutableList().apply(builder))
}

internal fun Task.awsCdkCmd(
    app: Provider<String>,
    profile: Provider<String>?,
    vararg args: String,
    builder: MutableList<String>.() -> Unit = {}
): ExecResult = awsCdkCmd(
    app = app.get(),
    profile = profile?.orNull,
    *args,
    builder = builder
)

internal fun Task.awsCdkCmd(
    app: String,
    profile: String?,
    vararg args: String,
    builder: MutableList<String>.() -> Unit = {}
) = cmd("cdk", *args, environment = mapOf("JSII_SILENCE_WARNING_UNTESTED_NODE_VERSION" to "1")) {
    builder()
    add("--app"); add(app)
    profile?.let { add("--profile"); add(it) }
}