package com.jespage.awscdk.templates

import PolicyBuilder
import com.jespage.awscdk.build
import software.amazon.awscdk.Duration
import software.amazon.awscdk.Stack
import software.amazon.awscdk.services.iam.Effect
import software.amazon.awscdk.services.iam.PolicyStatement
import software.amazon.awscdk.services.lambda.Architecture
import software.amazon.awscdk.services.lambda.Code
import software.amazon.awscdk.services.lambda.Function
import software.amazon.awscdk.services.lambda.Runtime

@Suppress("unused")
fun Stack.lambdaJava(
    id: String,
    functionName: String,
    codePath: String,
    handlerName: String = "Handler",
    memorySize: Int = 3008,
    timeout: Duration = Duration.minutes(15),
    runtime: Runtime = Runtime.JAVA_21,
    architecture: Architecture = Architecture.X86_64,
    environment: Map<String, String> = emptyMap(),
    functionBuilder: (Function.Builder).() -> Unit = {},
    inlinePolicy: (PolicyBuilder.() -> Unit)? = null
): Function {
    val lambda = Function.Builder.create(this, id).build {
        runtime(runtime)
        functionName(functionName)
        code(Code.fromAsset(codePath))
        handler(handlerName)
        timeout(timeout)
        memorySize(memorySize)
        architecture(architecture)
        retryAttempts(0)
        environment(mapOf("JAVA_TOOL_OPTIONS" to "-XX:+TieredCompilation -XX:TieredStopAtLevel=1") + environment)
        functionBuilder()
    }.apply {
        inlinePolicy?.let {
            PolicyBuilder().apply(it).statements.forEach { statement ->
                addToRolePolicy(PolicyStatement.Builder.create().build {
                    it.effect(Effect.ALLOW)
                    it.actions(statement.actions)
                    it.resources(statement.resources)
                })
            }
        }
    }
    return lambda
}